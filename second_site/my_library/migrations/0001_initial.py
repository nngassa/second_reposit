# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.TextField(max_length=225)),
                ('author', models.TextField(max_length=225)),
                ('publisher', models.TextField(max_length=225)),
                ('isbn', models.TextField(max_length=225)),
                ('check_out', models.BooleanField()),
                ('edition', models.TextField(max_length=20)),
            ],
        ),
    ]
