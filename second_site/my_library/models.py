from django.db import models

class Book(models.Model):
    title = models.TextField(max_length=225)
    author= models.TextField(max_length=225)
    publisher = models.TextField(max_length=225)
    isbn = models.TextField(max_length=225)
    check_out = models.BooleanField()
    edition = models.TextField(max_length=20) 
    
    def __str__(self):
        return "Title: %s, Author: %s"  %(self.title, self.author)
        

# Create your models here.
